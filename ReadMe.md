## Centroidal Voronoi Tessellation (CVT) Dataset Generator

This is a [CMake]()-ified version of [Paul Burkhardt's CVT dataset generator]().
The original library is licensed under the [LGPL]().
Any modifications or additions are licensed under the modified BSD license,
meaning that they (the modifications) may be reused under the more liberal
BSD license but you must adhere to the LGPL when using the CVT library.

[Paul Burkhardt's CVT dataset generator]: http://people.sc.fsu.edu/~jburkardt/cpp_src/cvt_dataset/cvt_dataset.html
[LGPL]: http://people.sc.fsu.edu/~jburkardt/txt/gnu_lgpl.txt
[CMake]: http://cmake.org/
